import 'package:flutter/material.dart';
import 'package:flutter_ble_scale/stepper.dart';

void main() {
  runApp(MyApp());
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(home: HomePage());
  }
}

class HomePage extends StatelessWidget {
  final title = "Home";

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text(title),
        ),
        body: new ListView(
            shrinkWrap: true,
            padding: const EdgeInsets.all(20.0),
            children: List.generate(choices.length, (index) {
              return Center(
                child: ChoiceCard(
                  choice: choices[index],
                  item: choices[index],
                  onTap: () {
                    Navigator.push(
                      context,
                      MaterialPageRoute(
                          builder: (context) => Detail(choice: choices[index])),
                    );
                  },
                ),
              );
            })));
  }
}

class Choice {
  const Choice({this.title, this.icon, this.body});
  final String title;
  final IconData icon;
  final Widget body;
}

List<Choice> choices = <Choice>[
  Choice(
    title: 'Bluetooth settings',
    icon: Icons.bluetooth,
    body: null,
  ),
  Choice(
    title: 'Weight',
    icon: Icons.local_dining,
    body: MyHome(),
  )
];

class ChoiceCard extends StatelessWidget {
  const ChoiceCard(
      {Key key,
      this.choice,
      this.onTap,
      @required this.item,
      this.selected: false})
      : super(key: key);

  final Choice choice;
  final VoidCallback onTap;
  final Choice item;
  final bool selected;

  @override
  Widget build(BuildContext context) {
    TextStyle textStyle = Theme.of(context).textTheme.headline4;
    if (selected) textStyle = textStyle.copyWith(color: Colors.blueGrey);
    return InkWell(
      onTap: () {
        onTap();
      },
      child: Card(
          color: Colors.white,
          child: Row(
            children: <Widget>[
              new Container(
                  padding: const EdgeInsets.all(25),
                  alignment: Alignment.topLeft,
                  child: Icon(
                    choice.icon,
                    size: 60.0,
                    color: textStyle.color,
                  )),
              new Expanded(
                  child: new Container(
                padding: const EdgeInsets.all(25.0),
                alignment: Alignment.centerLeft,
                child: Text(
                  choice.title,
                  style: TextStyle(
                    fontSize: 20.0,
                    color: Colors.black,
                  ),
                  textAlign: TextAlign.justify,
                  maxLines: 5,
                ),
              )),
            ],
            crossAxisAlignment: CrossAxisAlignment.start,
          )),
    );
  }
}

class Detail extends StatelessWidget {
  final Choice choice;
  Detail({this.choice});

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        appBar: AppBar(
          title: Text("${choice.title}"),
        ),
        body: choice.body);
  }
}
