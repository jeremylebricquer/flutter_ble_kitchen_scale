import 'package:flutter/material.dart';

class MyHome extends StatefulWidget {
  @override
  MyHomeState createState() => MyHomeState();
}

class MyHomeState extends State<MyHome> {
  // init the step to 0th position
  int currentStep = 0;
  List<Step> mySteps = [
    Step(title: Text("Pesée"), content: Text("Pesée"), isActive: true),
    Step(
        title: Text("En cours"),
        content: Text("En cours"),
/*         state: StepState.editing, */
        isActive: true),
    Step(title: Text("Fin de Pesée"), content: Text("Fini!!!"), isActive: true),
  ];

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Container(
          child: Stepper(
        currentStep: this.currentStep,
        steps: mySteps,
        type: StepperType.horizontal,
        onStepTapped: (step) {
          setState(() {
            currentStep = step;
          });
          print("onStepTapped : " + step.toString());
        },
        onStepCancel: () {
          setState(() {
            if (currentStep > 0) {
              currentStep = currentStep - 1;
            } else {
              currentStep = 0;
            }
          });
          print("onStepCancel : " + currentStep.toString());
        },
        onStepContinue: () {
          setState(() {
            if (currentStep < mySteps.length - 1) {
              currentStep = currentStep + 1;
            } else {
              currentStep = 0;
            }
          });
          print("onStepContinue : " + currentStep.toString());
        },
      )),
    );
  }
}
